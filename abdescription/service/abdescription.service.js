/**
 *
 *
 *
 * @Author: rwendlinger
 * @Email: rwendlinger@allbytes.de
 * @Date:   2016-08-22 09:52:56
 * @Last Modified by:   rwendlinger
 * @Last Modified time: 2016-11-02 20:24:36
 */
(function(){

'use strict';

angular.module('abdescriptionModule')
	.service('abdescriptionService', abdescriptionService);

abdescriptionService.$inject = [
	'$q',
	'abdescriptionAPI',
	'$filter'
];

function abdescriptionService($q, abdescriptionAPI, $filter) {
	var vm = {};
	vm.getAbdescriptionList = getAbdescriptionList;
	vm.getDescriptionByKey = getDescriptionByKey;
	vm.abdescriptionList = {};
	vm.activationDone = {};



	activate();

	function getAbdescriptionList(){
		return vm.abdescriptionList;
	}
	function getDescriptionByKey(key){
		var deferred = $q.defer();
		vm.activationDone.then(function(){
			var description = vm.abdescriptionList[key];
			if(typeof description == 'undefined'){
				deferred.reject();
			}
			else{
				deferred.resolve(description);
			}
		});

		return deferred.promise;
	}


	function activate() {
		var api = new abdescriptionAPI();
		vm.activationDone = api.getList();
		vm.activationDone.then(function(res) {
			vm.abdescriptionList = res.data.data;
		});
	}

}

})();