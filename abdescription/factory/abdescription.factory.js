/**
 *
 *
 *
 * @Author: rwendlinger
 * @Email: rwendlinger@allbytes.de
 * @Date:   2016-08-22 09:52:37
 * @Last Modified by:   rwendlinger
 * @Last Modified time: 2016-11-02 20:27:40
 */
(function(){

'use strict';

angular.module('abdescriptionModule')
	.factory('abdescriptionAPI', abdescriptionAPI);

abdescriptionAPI.$inject = [
	'$http',
	'abdescriptionConfig'
];
/**
 * Factory for abdescription
 */
function abdescriptionAPI($http, abdescriptionConfig) {
	function abdescriptionAPI(data){
		angular.extend(this, data);
		this.getList = getList;
	}
	function getList() {
		return $http.get(abdescriptionConfig.apiUrl);
	}

	return abdescriptionAPI;
	
}

})();