/**
 *
 *
 *
 * @Author: Roman Wendlinger
 * @Email: rwendlinger@allbytes.de
 * @Date:   2016-09-12 11:20:08
 * @Last Modified by:   rwendlinger
 * @Last Modified time: 2016-11-02 20:23:56
 */
(function(){

'use strict';


/**
* @desc 
* @example <div ab-description></div>
*/
angular
    .module('abdescriptionModule')
    .directive('abDescription', abdescription);

abdescriptionController.$inject = [
	'abdescriptionService'
];

function abdescription() {
    var directive = {
        link: link,
        
        transclude: true,
        restrict: 'A',
        
        controller: abdescriptionController,
        controllerAs: 'vm',
        bindToController: true
    };
    return directive;

    function link(scope, element, attrs, vm) {
        var key = attrs.abDescription;
        vm.getDescriptionByKey(key).then(function(data) {
            $(element).popover({
                content: data,
                title: key,
                trigger: 'hover'
            });
            $(element).addClass('abbr');
        },function() {
            console.error('"%s" not found in description', key);
        });
    }
}

function abdescriptionController(abdescriptionService){
    var vm = {};
    vm.abdescriptionService = abdescriptionService;
    vm.getDescriptionByKey = getDescriptionByKey;
    activate();

    function getDescriptionByKey(key){
        return abdescriptionService.getDescriptionByKey(key);
    }
    function activate(){    

    }
    

}
})();