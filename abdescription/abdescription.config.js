/**
 *
 *
 *
 * @Author: rwendlinger
 * @Email: rwendlinger@allbytes.de
 * @Date:   2016-09-07 16:39:51
 * @Last Modified by:   rwendlinger
 * @Last Modified time: 2016-09-12 13:08:44
 */
(function(){

'use strict'

angular.module('abdescriptionModule')
	.constant('abdescriptionConfig', {
		'modulePath': 'js/angular/app/abdescriptionModule/',
		'apiUrl': '/ABDescription'
	});


})();