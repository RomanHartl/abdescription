# AB-Description

Der Sinn dieses Modul ist es, Beschreibungen von Schlüsselwörtern in einer Anwendung vom Backend Server zu laden und als kleines Twitter-Bootstrap "popover" darzustellen. 

Der Key wird hierbei als Überschrift behandelt, die Beschreibung als Inhalt. 

Falls eine Beschreibung existiert, wird ein popover erstellt, welches bei "hover" reagiert, und die Klasse "abbr" wird hinzugefügt. Dies ermöglicht es, abweichende Darstellung via CSS selbst zu erstellen.

Falls keine Beschreibung existiert, wird auch kein popover dargestellt. Lediglich ein *console.error* weißt auf eine nicht vorhandene Description hin.

![Beispiel](beispiel.png)